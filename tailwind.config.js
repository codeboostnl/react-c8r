module.exports = {
  purge: [
    './src/**/*.js',
    './src/**/*.jsx',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        sans: ['Roboto'],
      },
    },
  },
  variants: {
    extend: {
      ringColor: ['hover'],
      // ringOffsetColor: ['hover'],
      // ringOffsetWidth: ['hover'],
      // ringOpacity: ['hover'],
      ringWidth: ['hover'],
    },
  },
  plugins: [],
}
