import React from 'react';

export default function Button({
                                   children = '?',
                                   width = 1,
                                   operation = false,
                                   equals = false,
                                   onClick = () => null
                               }) {
    return (
        <button
            onClick={onClick}
            className={`
                font-light
                flex items-center justify-center
                col-span-${width}
                w-${width * 16}
                h-16
                hover:ring-4
                hover:ring-inset
                text-left
                bg-gradient-to-b
                border-t border-r border-gray-800
                ${
                    operation
                        ? 'hover:ring-gray-400 from-gray-200 to-gray-300'
                        : equals
                            ? 'hover:ring-blue-400 from-blue-300 to-blue-400'
                            : 'hover:ring-gray-300 from-gray-100 to-gray-200'
                }
            `}
        >
            {children}
        </button>
    );
}
