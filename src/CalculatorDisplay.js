import React from 'react';

export default function Display({ value, error }) {
    return <div className={`
        Display
        flex
        items-end
        justify-end
        h-32
        text-7xl
        text-white
        p-2
        font-thin
        ${ error ? 'text-red-600' : ''}
    `}>
        <svg viewBox="0 0 128 128">
            <text x="0" y="26" textAnchor="middle" className="text-white">{value}</text>
        </svg>
    </div>;
}
