import React, {useState} from "react";
import { evaluate } from 'mathjs';
import Display from "./CalculatorDisplay";
import Button from "./CalculatorButton";

function Calculator() {
    const OPERATION_DIVISION = '/'
    const OPERATION_MULTIPLY = '*'
    const OPERATION_SUBTRACT = '-'
    const OPERATION_ADD = '+'
    const OPERATION_EQUALS = '='
    const OPERATION_INVERT = ''
    const OPERATION_PERCENT = '%'
    const OPERATION_ALL_CANCEL = 'ac'

    const [ value, setValue ] = useState('0');
    const [ expression, setExpression ] = useState('');
    const [ error, setError ] = useState(false);

    const solver = (input) => {
        const percentageRegex = /[0-9]*\.?[0-9]%/g,
              modulusRegex = /[0-9]*\.?[0-9]%[0-9]*\.?[0-9]/g,
              numbersRegex = /[0-9]*\.?[0-9]/g;

        // Resolve modulus expressions
        if (input.match(modulusRegex)) {
            const modulusValues = input.match(modulusRegex);

            modulusValues.forEach((modulusValue) => {
                input = input.replace(modulusValue, String(evaluate(modulusValue)));
            });
        }

        // Resolve percentage expressions
        while (input.match(percentageRegex) && !input.match(modulusRegex)) {
            const right = input.match(percentageRegex)[0],
                  left = input.split(right)[0],
                  leftTrimmed = left.trim();

            console.log('right', right)
            console.log('left', left)

            const leftValue = leftTrimmed.slice(0, -1);

            const operator = leftTrimmed.slice(-1);
            console.log('operator', operator)

            const percentage = right.match(numbersRegex)[0]
            console.log('percentage', percentage)

            const rightExpression = leftValue + '*' + percentage + '/100';
            console.log('rightExpression', rightExpression)

            const rightValue = String(evaluate(rightExpression));
            console.log('rightValue', rightValue)

            console.log(left + right, '->', left + rightValue)
            input = input.replace(left + right, left + rightValue);

            console.log('input', input)
        }

        return evaluate(input);
    }

    const solve = (expression) => {
        const parenthesesRegex = /\([^)]+\)/;

        let input = expression;

        // Resolve parenthesized groups
        while (input.match(parenthesesRegex)) {
            // Find the most right group
            const groupStart = input.substring(input.lastIndexOf('('));
            const group = groupStart.substring(0, groupStart.indexOf(')') + 1);

            if (group.length === 0) {
                break;
            }

            // Find the group expression to solve
            const groupExpressionStart = input.substring(input.lastIndexOf('(') + 1);
            const groupExpression = groupExpressionStart.substring(0, groupExpressionStart.indexOf(')'));

            // Resolve the parenthesized group expression
            let groupValue = solver(groupExpression);

            // Replace the parenthesized group with the solved value
            input = input.replace(group, groupValue);
        }

        return solver(input);

    }

    const handleInput = (input) => {
        switch(input) {
            case OPERATION_EQUALS: {
                try {
                    const result = solve(expression);

                    setValue(String(result))
                    setExpression(String(result))
                } catch (error) {
                    setError(true);
                }

                break;
            }
            case OPERATION_ALL_CANCEL: {
                setExpression('')
                setValue('0')
                break
            }
            case OPERATION_INVERT: {
                if (Number(value)) {
                    setValue(value => String(value * -1))
                }

                break
            }
            case '.': {
                if (!value.includes('.')) {
                    const value = expression + '.';
                    setExpression(value)
                    setValue(value)
                }
                break;
            }
            default: {
                const value = expression + input;
                setExpression(value)
                setValue(String(value))
                break;
            }
        }
    }

    return (
        <div className="w-64 bg-gray-800 shadow-lg ring-1 ring-gray-800">
            <Display value={value} error={error} />
            <div className="grid grid-cols-4 gap-0 text-2xl border-gray-800 border-l border-b">
                <Button onClick={() => handleInput('(')} operation>(</Button>
                <Button onClick={() => handleInput(')')} operation>)</Button>
                <Button onClick={() => handleInput(OPERATION_PERCENT)} operation>%</Button>
                <Button onClick={() => handleInput(OPERATION_ALL_CANCEL)} operation>AC</Button>


                <Button onClick={() => handleInput(7)}>7</Button>
                <Button onClick={() => handleInput(8)}>8</Button>
                <Button onClick={() => handleInput(9)}>9</Button>
                <Button onClick={() => handleInput(OPERATION_DIVISION)} operation>∕</Button>


                <Button onClick={() => handleInput(4)}>4</Button>
                <Button onClick={() => handleInput(5)}>5</Button>
                <Button onClick={() => handleInput(6)}>6</Button>
                <Button onClick={() => handleInput(OPERATION_MULTIPLY)} operation>×</Button>


                <Button onClick={() => handleInput(1)}>1</Button>
                <Button onClick={() => handleInput(2)}>2</Button>
                <Button onClick={() => handleInput(3)}>3</Button>
                <Button onClick={() => handleInput(OPERATION_SUBTRACT)}operation>−</Button>


                <Button onClick={() => handleInput(0)}>0</Button>
                <Button onClick={() => handleInput('.')}>.</Button>
                <Button onClick={() => handleInput(OPERATION_EQUALS)} equals>=</Button>
                <Button onClick={() => handleInput(OPERATION_ADD)} operation>+</Button>


            </div>
        </div>
    );
}

export default Calculator;
