<h1>react-c8r 🧮</h1>
<p>
  <img alt="Version" src="https://img.shields.io/npm/v/react-c8r?style=flat-square&logo=npm" />

  <img alt="Peer dependency: react" src="https://img.shields.io/npm/dependency-version/react-c8r/peer/react?style=flat-square&logo=npm" />
  <img alt="Peer dependency: react-dom" src="https://img.shields.io/npm/dependency-version/react-c8r/peer/react-dom?style=flat-square&logo=npm" />
  <img alt="Dependency: mathjs" src="https://img.shields.io/npm/dependency-version/react-c8r/mathjs?style=flat-square&logo=npm" />
  <a href="https://www.apache.org/licenses/LICENSE-2.0" target="_blank">
    <img alt="License: Apache-2.0" src="https://img.shields.io/npm/l/react-c8r?style=flat-square" />
  </a>
</p>

A React calculator component.

## Installation

The package can be installed via [npm](https://github.com/npm/cli):

```sh
npm install react-c8r
```

Or via [yarn](https://github.com/yarnpkg/yarn):

```sh
yarn add react-c8r
```
## Usage

Here's an example of basic usage:

```js
import React from 'react';
import Calculator from 'react-c8r';

const Example = () => {
  return (
    <Calculator />
  );
}

```

## Testing

```sh
npm run test
```

## License

The [Apache-2.0 License](https://www.apache.org/licenses/LICENSE-2.0).

## Author

👤 **Code Boost <daniel@codeboost.nl> (https://codeboost.nl)**

